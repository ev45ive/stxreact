//import path from 'path'
var path = require('path')
var ExtractText = require('extract-text-webpack-plugin')
var HTMLWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: [
        './src/main.jsx',
       // './src/styles.css'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve:{
        extensions:['.jsx','.js']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use:ExtractText.extract({
                    use:'css-loader'
                })
            },{
                test:/\.(png|jpg|svg|gif)$/,
                use: {
                    loader: 'file-loader',
                    options:{
                        name:'assets/[name].[hash].[ext]'
                    }
                },
            }
        ]
    },
    plugins:[
        new ExtractText({filename:'bundle.css'}),
        new HTMLWebpackPlugin({template:'./src/index.html'}),
        // new HTMLWebpackPlugin({template:'./src/index.html',filename:'admin.html',chunks:['admin']})
    ],
    devServer:{
        historyApiFallback:true,
        proxy:{
            // Django, REST, etc.
        }
    },
    devtool:'sourcemap',
}