import React from 'react';


export class Todos extends React.PureComponent {

    state = {
        newTitle: ''
    }

    onAdd = (event) => {
        event.preventDefault();
        
       this.props.addTodo(this.state.newTitle)

        this.setState({
            newTitle: ''
        })
    }

    onInput = (e) => {
        this.setState({
            newTitle: e.target.value
        })
    }

    onRemove(todoId) {
       this.props.removeTodo(todoId)
    }

    toggleCompleted(todoId) {
        this.props.toggleCompleted(todoId)
    }

    render() {
        console.log('todo render')
        return <div style={{ margin: '20px' }}>
            <div className="list-group">
                {this.props.todos.map((todo, index) =>
                    <div className="list-group-item" key={todo.id}>
                        <label>
                            <input type="checkbox" checked={todo.completed}
                                onChange={e => this.toggleCompleted(todo.id)} />
                            {index + 1 + '. ' + todo.title}
                        </label>
                        <span className="float-right" onClick={e => this.onRemove(todo.id)}> &times; </span>
                    </div>)}
            </div>
            <p> completed: {this.props.calculateCompleted} </p>
            <hr />
            <form onSubmit={this.onAdd}>
                <div className="input-group">
                    <input type="text" className="form-control" ref={elem => elem && elem.focus()} value={this.state.newTitle} onChange={this.onInput} />
                    <input type="button" className="btn btn-success" value="Add" onClick={this.onAdd} />
                </div>
            </form>
        </div>
    }
}