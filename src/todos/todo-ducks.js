
// ACTIONS
const ADD = 'TODOS/ADD'
const REMOVE = 'TODOS/REMOVE'
const TOGGLE_COMPLETED = 'TODOS/TOGGLE_COMPLETED'

const initial = []

export default (state = initial, action) => {
    switch (action.type) {
        case ADD:
            return {
                ...state,
                entities:{
                    ...state.entities,
                    [action.payload.id] : action.payload
                },
                list: [...state.list, action.payload.id]
            }
        case REMOVE:
            var entities = {...state.entities}
            delete entities[action.payload]
            return {
                ...state,
                list: state.list.filter( id => id != action.payload),
                entities
            }
        case TOGGLE_COMPLETED:
            var todo = state.entities[action.payload]
            todo = {
                ...todo,
                completed: !todo.completed
            }
            return {
                ...state,
                entities:{
                    ...state.entities,
                    [action.payload] :todo
                }
            }
        default:
            return state
    }
}

export const addTodo = title => ({
    type: ADD,
    payload: {
        id: Date.now(),
        title,
        completed: false
    }
})

export const removeTodo = id => ({
    type: REMOVE,
    payload: id
})

export const toggleCompleted = id => ({
    type: TOGGLE_COMPLETED,
    payload: id
})