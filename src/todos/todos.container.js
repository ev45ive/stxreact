import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Todos} from './todos'
import {addTodo,removeTodo,toggleCompleted} from '../todos/todo-ducks'
import { createSelector } from 'reselect'

const getTodosList = createSelector(
    state => state.todos.list,
    state => state.todos.entities,
    (list,entities) => {
     console.log('reselect')
     return   list.map(id => entities[id])
    }
)

const calculateCompleted = createSelector(
    state => getTodosList(state),
    todos => todos.filter(todo => todo.completed).length
)

const mapStateToProps = (state, ownProps)  => ({
    ...ownProps,
    todos: getTodosList(state),
    calculateCompleted: calculateCompleted(state)
})

const mapDispatchToProps = (dispatch, ownProps) => bindActionCreators({
    addTodo,
    removeTodo,
    toggleCompleted
    //toggleCompleted: id => toggleCompleted(id, 'extra_params')
},dispatch)

// const mapDispatchToProps = (dispatch, ownProps) => ({
//     removeTodo: todoId => dispatch(removeTodo(todoId)),
//     addTodo: todo => dispatch(addTodo(todo))
//  })

export default connect(mapStateToProps,mapDispatchToProps)(Todos)