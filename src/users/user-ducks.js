const SELECT = 'USERS/SELECT'
const SAVE = 'USERS/SAVE'
const FETCH = 'USERS/FETCH'

import { createAction, handleActions } from 'redux-actions'

export const selectUser = createAction(SELECT, id => id)
/* 
    export const selectUser = id => ({
        type: SELECT,
        payload: id 
    }) 
*/

export const saveUser = user => {
    return dispatch => {

        dispatch({
            type: 'USERS/FETCH_SAVE',
            payload: fetch('http://localhost:3000/users/' + user.id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            }).then(response =>
                response.ok ? response.json() : 'error'
            )
        })
        .then(user => {
            // dispatch({
            //     type: SAVE,
            //     payload: user
            // })
            dispatch(fetchUsers())
        })
    }
}

export default (state = {
    entities: {},
    list: [],
    selected: null,
}, action) => {

    switch (action.type) {
        case SELECT:
            return {
                ...state,
                selected: action.payload
            }
        case SAVE:
            return {
                ...state,
                entities: {
                    ...state.entities,
                    [action.payload.id]: action.payload
                }
            }
        case 'USERS/FETCH_PENDING':
            return {
                ...state,
                error: '',
                loading: true
            }
        case 'USERS/FETCH_REJECTED':
            return {
                ...state,
                error: action.payload,
                loading: false
            }
        case 'USERS/FETCH_FULFILLED':
            return {
                ...state,
                error: '',
                loading: false,
                list: action.payload.map(user => user.id),
                entities: action.payload.reduce((entities, user) => {
                    entities[user.id] = user;
                    return entities;
                }, {})
            }
        default:
            return state;
    }
}


export const fetchUsers = () => {
    return {
        type: 'USERS/FETCH',
        payload: fetch('http://localhost:3000/users')
            .then(response => {
                return response.ok ? response.json() :
                    Promise.reject(response.statusText)
            })
            .then(users => users.map(user => ({
                bio: '',
                active: false,
                ...user
            })))
    }
}


// export const fetchUsers = () => {
//     return dispatch => {
//         fetch('http://localhost:3000/users')
//             .then(response => {
//                 return response.ok ? response.json() :
//                     Promise.reject(response.statusText)
//             })
//             .then(users => users.map(user => ({
//                 bio: '',
//                 active: false,
//                 ...user
//             })))
//             .then(users => {
//                 dispatch({
//                     type: 'USERS/FETCH_SUCCESS',
//                     payload: users
//                 })
//             })
//             .catch(err => {
//                 dispatch({
//                     type: 'USERS/FETCH_FAIL',
//                     payload: err
//                 })
//             })

//         return {
//             type: 'USERS/FETCH_START'
//         }
//     }
// }


// export default handleActions({
//     [selectUser]: (state,action)=>{
//         debugger
//     },
//     [saveUser]: (state,action)=>{
//         debugger
//     }
// },{})
// window.selectUser = selectUser;