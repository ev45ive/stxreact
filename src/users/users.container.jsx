import { connect } from 'react-redux'
import {Users} from './users'
import {selectUser,saveUser,fetchUsers} from './user-ducks'
import {createSelector} from 'reselect'
import {bindActionCreators} from 'redux'

const getUsers = createSelector(
    state => state['users'].list,
    state => state['users'].entities,
    (list, entities) => list.map(id => entities[id])
)

const mapStateToProps = (state, ownProps) => ({
    ...ownProps,
    users: getUsers(state),
    selected: state.users.entities[state.users.selected],loading: state.users.loading,
    error: state.users.error
})

const mapDispatchToProps = (dispatch, ownProps) => bindActionCreators({
    selectUser,
    saveUser,
    fetchUsers
},dispatch)

// const mergeProps = (stateProps, dispatchProps, ownProps) => {
//     return {
//         mergeProp: mergePropVal
//     }
// }

export default connect(mapStateToProps, mapDispatchToProps)(Users)