import React from 'react';

export const UsersList = props =>
 <table className="table table-hover">
    <tbody> 
        {props.users.map( user => {
            const selected = props.selected == user.id? 'table-active': '';

            return <tr key={user.id} 
                className={`jakas-klasa ${selected}`}
                onClick={e => props.onSelect(user.id)}    
            >
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
            </tr>
        })}
    </tbody>
</table>