import React from 'react'
import PropTypes from 'prop-types'

export class UserForm extends React.Component {

    state = {

    }

    static propTypes = {
        onSave: PropTypes.func.isRequired,
        // user: PropTypes.shape({
        //     name: PropTypes.string,
        //     active: PropTypes.bool
        // })
    }

    static defaultProps = {
        user:{},
        // onSave: ()=>{
        //     throw "User form requires onSave property"
        // }
    }

    constructor(props) {
        super(props)
        this.state.user = props.user;
    }

    // componentWillMount(...args){
    //     console.log('componentWillMount', args)
    // }

    // componentWillUnmount(...args){
    //     console.log('componentWillUnmount', args)
    // }
    // shouldComponentUpdate(props,state){
    //     console.log('shouldComponentUpdate', props)
    //     return true
    // }
    // componentWillUpdate(...args){
    //     console.log('componentWillUpdate', args)
    // }
    // componentDidUpdate(...args){
    //     console.log('componentDidUpdate', args)
    // }

    // componentDidMount(...args){
    //     console.log('componentDidMount', args)
    // }

    componentWillReceiveProps(props) {
        this.setState({
            user: props.user
        })
    }

    onInput = (e) => {
        const target = e.target
        const value = target.type == 'checkbox'? target.checked : target.value
        this.setState({
            user: {
                ...this.state.user,
                [e.target.name]: value
            }
        })
    }
    
    onSave = (e) => {
        e.preventDefault()
        this.props.onSave(this.state.user)
    }

    render() {
        const user = this.state.user;

        return <div>
            {/* form>div.form-group>label{Name}+input.form-control         */}
            <form onSubmit={this.onSave}>
                <div className="form-group">
                    <label>Name</label>
                    <input type="text" name="name" className="form-control"
                        value={user.name} onChange={this.onInput} />
                </div>
                <div className="form-group">
                    <label>E-mail</label>
                    <input type="text" name="email" className="form-control"
                        value={user.email} onChange={this.onInput} />
                </div>
                {/* <div className="form-group">
                    <label>Active</label>
                    <input type="checkbox" name="active"
                        checked={user.active} onChange={this.onInput} />
                </div>
                <div className="form-group">
                    <label>Bio</label>
                    <textarea name="bio"  className="form-control" value={user.bio} onChange={this.onInput} />
                </div> */}
                <button className="btn btn-success">Save</button>
            </form>
        </div>
    }
}