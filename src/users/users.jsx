import React from 'react';
import PropTypes from 'prop-types'

import { UsersList } from './users-list'
import { UserForm } from './user-form'
import { Route } from 'react-router-dom'

export class Users extends React.Component {

    static defaultProps = {
        users: [],
        selected: null
    }

    selectUser = (selected) => {
        this.props.selectUser(selected)
    }


    saveUser = (updatedUser) => {
        this.props.saveUser(updatedUser)
    }

    fetchUsers = () => {
        this.props.fetchUsers()
    }

    // componentDidMount(){
    //     this.fetchUsers()
    // }

    render() {
        return <div>
            <h3>Users</h3>
            <div className="row">
                <div className="col">
                    {this.props.loading ?
                        <div>Loading...</div>
                        :
                        (this.props.error || <div>
                            <button onClick={this.fetchUsers}>Fetch</button>
                            <UsersList
                                users={this.props.users}
                                selected={this.props.selected}
                                onSelect={this.selectUser} />
                        </div>)
                    }
                </div>
                <div className="col">
                    <Route path="/users/:id" render={(router) => {
                        console.log(this.props)
                        return <div>
                            {router.match.params['id']}
                            {this.props.selected? <UserForm user={this.props.selected} onSave={this.saveUser} /> : 'Please select user' }
                        </div>
                    }} />
                </div>
            </div>
        </div>
    }
}