import './styles.css';

import React from 'react';
import ReactDOM from 'react-dom'
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'

import { Provider } from 'react-redux'
import { store } from './stores/redux-store'

import { App } from './app'
import { Layout } from './components/layout'

ReactDOM.render(
<Router>
    {/* <Route component={Users} path="/users"> */}
    <Provider store={store}>
        <Layout title="Szkolenie">
            <App />
        </Layout>
    </Provider>
</Router>
, document.getElementById('app-root'))


