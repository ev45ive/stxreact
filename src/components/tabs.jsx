import React from 'react'

export class Tabs extends React.Component {

    state = {
        active: 'users'
    }

    setActive = (e,active) => {
        e.preventDefault()
        this.setState({
            active
        })
    }


    
    render() {
        let tabs = this.props.children;

        let children = tabs.filter(tab =>
           tab.key == this.state.active
        )

        return <div>
            <ul className="nav nav-tabs mb-2">
                {tabs.map( tab => 
                <li className="nav-item" key={tab.key} >
                    <a href="#"
                    className={`nav-link ${tab.key == this.state.active? 'active' : ''}`} 
                    onClick={e => this.setActive(e,tab.key)}>
                        {tab.props.label}
                    </a>
                </li>
                )}
            </ul>
            <div>{children}</div>
        </div>
    }
}

export const Tab = props => <div>
    {props.children}
</div>