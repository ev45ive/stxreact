import React from 'react';

export const ListItem = props => <div className="list-group-item">{props.title}</div>

export const ItemsList = ({title, items}) => <div>
    <h3>{title}</h3>
    <div className="list-group">
        { items.map( item => <ListItem key={item.id} title={item.title} /> ) }
    </div>
</div>