import React from 'react'

export const Layout = props => <div className="container">
    <div className="row">
        <div className="col">
                <h1>{props.title}</h1>
                {props.children}
        </div>
    </div>
</div> 