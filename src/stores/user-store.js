import {EventEmitter} from 'events'

export class UsersStore extends EventEmitter{

    EVENT_TYPE = 'CHANGE'

    state = {
        users:[],
        selected: null
    }

    subscribe(listener){
        this.on(this.EVENT_TYPE,listener)
        return listener;
    }

    unsubscribe(listener){
        this.removeListener(this.EVENT_TYPE, listener)
    }

    notify(){
        this.emit(this.EVENT_TYPE)
    }

    dispatch(action){
        switch(action.type){
            case 'FETCH_USERS':
                this.fetchUsers()
                break;
            case 'SAVE_USER':
                this.saveUser(action.payload)
            
        }
    }

    setState(state){
        this.state = {
            ...this.state,
            ...state
        }
        this.notify()
    }

    getState(){
        return this.state
    }
    /////////////////////

    selectUser(selected){
        this.setState({
            selected
        })
    }

    fetchUsers(){
        
        fetch('http://localhost:3000/users/')
        .then( response => response.json() )
        .then( users => users.map( user => ({
            bio:'',
            active:false,
            ...user
        })))
        .then( users => this.state.users = users )
        .then(() => this.notify() )
    } 

    saveUser(updatedUser){
        fetch('http://localhost:3000/users/' + updatedUser.id ,{
            method:'PUT',
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify(updatedUser)
        }).then( response => {
            response.ok? this.fetchUsers() : 'error'
        })
    }

}