import {createStore, combineReducers,applyMiddleware} from 'redux'
import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'

const initialState = {
    todos: {
        entities:{
            1:  { id: 1, title: 'Todo 1', completed: true },
            2:  { id: 2, title: 'Todo 2', completed: false }
        },
        list: [1,2]
    },
    users:{
        entities:{
            1: {
                id: 1,
                name: 'Test', 
                email: 'test@test',
                active: false
            },
            2: {
                id: 2,
                name: 'alice', 
                email: 'alice@test',
                active: true
            }

        },
        list:[1,2],
        selected: null
    }
}
/* TODO:
    - users reducer + actions ( selectUser, saveUser )
    - users container +mapState + mapDispatch + reselectors
    - ! fetchUsers()
*/


const counterReducer = (state = 0, action) => {

    switch(action.type){
        case 'INC':
            return state + action.payload

        case 'DEC':
            return state - action.payload
         default:
            return state
    }
}


import todos from '../todos/todo-ducks';
import users from '../users/user-ducks';

const rootReducer = combineReducers({
    counter: counterReducer,
    todos,
    users
})

const loggerMiddleware = store => next => action => {
    console.log('ACTION',action);
    var state = next(action)
    
    console.log('STATE',state);
    return state;
}

// const functionMiddleware = store => next => action => {

//     if('function' == typeof action){
//         return next( action(store.dispatch) )
//     }
//     return next(action)
// }


export const store = createStore(rootReducer,initialState,applyMiddleware(
    loggerMiddleware,
    promiseMiddleware(),
    thunkMiddleware,
))

///////////
/// DEBUG:
    window.store = store;
    // store.subscribe( () => {
    //     console.log('LOG:', store.getState() )
    // })
///
