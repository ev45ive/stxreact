import React from 'react';

import Users from './users/users.container'
import Todos from './todos/todos.container'
import { Tabs, Tab } from './components/tabs'
import { Route, Redirect, Switch, Link, NavLink} from 'react-router-dom'

export class App extends React.Component {

    state = {
        test: ''
    }

    constructor() {
        super()
    }

    render() {
        return <div>
            <div className="row">
                <div className="col">
                    <ul className="nav nav-tabs mb-3">
                        <li className="nav-item">
                            <NavLink className="nav-link"
                            activeClassName="active" to="/users">Users</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" 
                            activeClassName="active" to="/todos">Todos</NavLink>
                        </li>
                    </ul>
                </div>
            </div><div className="row">
                <div className="col">
                    {this.context.authenticated &&
                        <Redirect to="/login" />
                    }

                    <Switch>
                        <Route path="/" exact render={() =>
                            <Redirect to="/users" />
                        } />
                        <Route path="/users" component={Users} />
                        <Route path="/todos" component={Todos} />
                        <Route render={() =>
                            <div> 404 - wrong url! </div>
                        } />
                    </Switch>
                </div>
            </div>
        </div>
    }
}